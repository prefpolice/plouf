$(document).ready(function(){
    var $collectionHolder = $('#inscription_participants');
    $('#inscription_nbParticipant').change(function () {
        removeAll($collectionHolder);
        $collectionHolder.data('index', $collectionHolder.find(':input').length);
        var $nb_personnes = $('#inscription_nbParticipant option:selected').val();
        if ($nb_personnes == 1) {
            addTagForm($collectionHolder);
        } else if ($nb_personnes == 2) {
            addTagForm($collectionHolder);
            addTagForm($collectionHolder);
        } else if ($nb_personnes == 3) {
            addTagForm($collectionHolder);
            addTagForm($collectionHolder);
            addTagForm($collectionHolder);
        } else if ($nb_personnes == 4) {
            addTagForm($collectionHolder);
            addTagForm($collectionHolder);
            addTagForm($collectionHolder);
            addTagForm($collectionHolder);
        } else {
            removeAll($collectionHolder);
        }
    });
     //
     function addTagForm($collectionHolder) {
                // Get the data-prototype explained earlier
                var prototype = $collectionHolder.data('prototype');

                // get the new index
                var index = $collectionHolder.data('index');
                // Replace '__name__' in the prototype's HTML to
                // instead be a number based on how many items we have
                var newForm = prototype.replace(/__name__/g, index);


                // increase the index with one for the next item
                $collectionHolder.data('index', index + 1);

                // Display the form in the page in an li, before the "Add a tag" link li
                $($collectionHolder).append(newForm);

            }
     function removeAll($div) {
            $($div).children().remove();
     }
    $centre = $('#inscription_centre');
    $('#inscription_jour').parent("div").hide();
    $('#inscription_horaire').parent("div").hide();
    $centre.change(function() {
        var $jour = $('#inscription_jour');
        // ... retrieve the corresponding form.
        var $form = $(this).closest('form');
        // Simulate form data, but only include the selected sport value.
        var data = {};
        data[$centre.attr('name')] = $centre.val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(html) {
                // Replace current position field ...
                $('#inscription_jour').replaceWith(
                    // ... with the returned one from the AJAX response.
                    $(html).find('#inscription_jour')
                );
                $('#inscription_jour').parent("div").show();

                $jour = $('#inscription_jour');
                // When sport gets selected ...
                $jour.change(function() {
                    // ... retrieve the corresponding form.
                    var $form = $(this).closest('form');
                    // Simulate form data, but only include the selected sport value.
                    var data = {};
                    data[$jour.attr('name')] = $jour.val();
                    // Submit data via AJAX to the form's action path.
                    $.ajax({
                        url : $form.attr('action'),
                        type: $form.attr('method'),
                        data : data,
                        success: function(html) {
                            // Replace current position field ...
                            $('#inscription_horaire').replaceWith(
                                // ... with the returned one from the AJAX response.
                                $(html).find('#inscription_horaire')
                            );
                            $('#inscription_horaire').parent("div").show();

                            // Position field now displays the appropriate positions.
                        }
                    });
                });
            }
        });
    });
});