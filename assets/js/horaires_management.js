'use_strict';
require ('../vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
require ('../vendor/bootstrap-timepicker/bootstrap-timepicker');
require ('./collection');
$(document).ready(function(){
    $('#jour_type_horaires').collection({
        up: false,
        down: false,
        add: '<a href="#" class="collection-add btn btn-default" title="Ajouter"><span class="fas fa-plus"></span></a>',
        remove: '<a href="#" class="collection-remove btn btn-default" title="Supprimer"><span class="fas fa-trash"></span></a>',
        duplicate: false,
        after_add: function(collection, element) {
            // automatic backup or whatever
            $('.timepicker').timepicker({
                icons:{
                    up: 'fas fa-arrow-up',
                    down: 'fas fa-arrow-down'
                },
                showMeridian: false
            });
            return true;
        }
    });
});