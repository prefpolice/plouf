const routes = require('../../../public/js/fos_js_routes.json');
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

Routing.setRoutingData(routes);

$(document).ready(function () {
    $("#inscription_atelier_type_atelier").change(function(){
        const $element = $(this).find(":selected");
        const atelier_id = $element.val();
        if (atelier_id){
            $.ajax({
                method: "POST",
                url: Routing.generate("atelier_get_infos",{"id": atelier_id})
            }).done(function(html){
                $(".infos").empty().html(html);
            })
        }else{
            $(".infos").empty();
        }

    });
});
