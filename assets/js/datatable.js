import 'datatables.net'
import 'datatables.net-bs4'
import 'datatables.net-buttons'
import 'datatables.net-buttons-bs4'
import '../vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake'
import '../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5'
import '../vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip'
import 'datatables.net-buttons/js/buttons.flash';
import 'datatables.net-buttons/js/buttons.colVis';
import 'datatables.net-buttons/js/buttons.print';

(function($) {

    'use strict';
    var datatableInit = function() {
        var $table = $('#datatable-tabletools');
        $table.dataTable({
            sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
            dom: 'Bfrtip',
            buttons: [
                { extend: 'print', text: 'Imprimer' },
                { extend: 'pdfHtml5', text: 'PDF' },
                { extend: 'colvis', text: 'Affichage' }
            ],
            "language": {
                "processing":     "Traitement en cours...",
                "search":         "Rechercher&nbsp;:",
                "lengthMenu":    "Afficher _MENU_ &eacute;l&eacute;ments",
                "info":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "infoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                "infoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "infoPostFix":    "",
                "loadingRecords": "Chargement en cours...",
                "zeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "emptyTable":     "Aucune donnée disponible dans le tableau",
                "paginate": {
                    "first":      "Premier",
                    "previous":   "Pr&eacute;c&eacute;dent",
                    "next":       "Suivant",
                    "last":       "Dernier"
                },
                "aria": {
                    "sortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sortDescending": ": activer pour trier la colonne par ordre décroissant"
                }
            }

        });
        $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#datatable-tabletools_wrapper');
        $table.DataTable().buttons().container().prependTo( '#datatable-tabletools_wrapper .dt-buttons' );
        $('#datatable-tabletools_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
    };
    var datatableInit2 = function() {
        var $table = $('#datatable-tabletools-atelier');
        $table.dataTable({
            sDom: '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
            buttons: [
                { extend: 'print', text: 'Imprimer' }
            ],
            "language": {
                "processing":     "Traitement en cours...",
                "search":         "Rechercher&nbsp;:",
                "lengthMenu":    "Afficher _MENU_ &eacute;l&eacute;ments",
                "info":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "infoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                "infoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "infoPostFix":    "",
                "loadingRecords": "Chargement en cours...",
                "zeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "emptyTable":     "Aucune donnée disponible dans le tableau",
                "paginate": {
                    "first":      "Premier",
                    "previous":   "Pr&eacute;c&eacute;dent",
                    "next":       "Suivant",
                    "last":       "Dernier"
                },
                "aria": {
                    "sortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sortDescending": ": activer pour trier la colonne par ordre décroissant"
                }
            }

        });
        $('<div />').addClass('dt-buttons mb-2 pb-1 text-right').prependTo('#datatable-tabletools-atelier_wrapper');
        $table.DataTable().buttons().container().prependTo( '#datatable-tabletools-atelier_wrapper .dt-buttons' );
        $('#datatable-tabletools-atelier_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');
    };
    $(function() {
        datatableInit();
        datatableInit2();
    });

}).apply(this, [jQuery]);

