var Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    .addEntry('admin', './assets/js/admin.js')
    .addEntry('app', './assets/js/app.js')
    .addEntry('collection', './assets/js/collection.js')
    .addEntry('horaires_management', './assets/js/horaires_management.js')
    .addEntry('datatable', './assets/js/datatable.js')
    .enableSingleRuntimeChunk()
    .addPlugin(new CopyWebpackPlugin([
        // copies to {output}/static
        { from: './assets/img', to: 'static' },
        { from: './components', to: 'components' },
    ]))
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables Sass/SCSS support
    .enableSassLoader()
    // uncomment if you're having problems with a jQuery plugin
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery'
    })


// uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
;
module.exports = Encore.getWebpackConfig();
