#Application Plouf

Plateforme de gestion des initiations à l'évenement plouf, gestion des jours de formation, des ateliers, des horaires avec back office pour une complète autonomie.

## Prérequis matériels et logiciel

Pour le bon fonctionnement de l'application, les prérequis sont:

* Un serveur web (Apache, Nginx)
* Une base de données MySql
* Php >= 7.2 ( avec les extensions )
* Composer ([plus d'informations](https://getcomposer.org/doc/00-intro.md))

Pour plus d'informations, rendez-vous sur le site de [sensiolabs](https://symfony.com/doc/current/reference/requirements.html)


## Installation

Pour installer l'applications plouf il faut : 

#### Préparer le fichier hôte sur le serveur

Utiliser la configuration recommandée par [sensiolabs](https://symfony.com/doc/current/setup/web_server_configuration.html).

Exemple pour le serveur Nginx:

```
server {
    server_name domain.tld www.domain.tld;
    root /var/www/project/public;

    location / {
        # try to serve file directly, fallback to index.php
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        # optionally set the value of the environment variables used in the application
        # fastcgi_param APP_ENV prod;
        # fastcgi_param APP_SECRET <app-secret-id>;
        # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";
        # fastcgi_param MAILER_URL "smtp://localhost:25?encryption=&auth_mode=";
        # fastcgi_param MAILER_USERNAME "emailAdmin@domaine.com";
        
        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/index.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/project_error.log;
    access_log /var/log/nginx/project_access.log;
}
``` 
Pour plus d'informations, reportez vous [à la documentation](https://symfony.com/doc/current/deployment.html)

#### Cloner le projet
```
~ git clone https://framagit.org/prefpolice/plouf.git --branch beta
```
Puis rendez vous dans le dossier plouf
```
cd plouf/
```

#### Installer les vendors et la base de données
Attention : Avant de lancer les commandes suivantes, assurez vous que les variables d'environnements relatives à la base de données sont bien configurées. 


```
~/plouf/: ./bin/console doctrine:database:create
```
Cette commande a pour effet de créer directement la base de données sans passer par MySql
```
~/plouf/: composer install
```
Cette commande aura pour effet d'installer les paquets nécéssaires, les configurer, et instancier la base de données.

#### Générer le compte administrateur et la configuration de base
Une commande spéciale permet de générer l'administrateur ainsi qu'une configuration par défaut et de l'informer par email que sont compte a bien été crée. 
```
~/plouf/: ./bin/console app:start
```

L'application est désormais opérationnelle et vous pouvez cous connecter en vous rendant sur votre url. 
```
http://votre_url.com/admin
```