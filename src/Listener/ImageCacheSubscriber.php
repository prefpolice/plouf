<?php

namespace App\Listener;

use App\Entity\Configuration;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ImageCacheSubscriber implements EventSubscriber
{

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;

    public function __construct(CacheManager $cacheManager, UploaderHelper $uploaderHelper)
    {
        $this->cacheManager = $cacheManager;
        $this->uploaderHelper = $uploaderHelper;
    }

    public function getSubscribedEvents()
    {
        return [
            'preRemove',
            'preUpdate'
        ];
    }

    public function preRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        if (!$entity instanceof Configuration) {
            return;
        }
        $this->cacheManager->remove($this->uploaderHelper->asset($entity->getBackgroundImage(), 'imageFile'));
        $this->cacheManager->remove($this->uploaderHelper->asset($entity->getAppLogo(), 'imageFile'));
        $this->cacheManager->remove($this->uploaderHelper->asset($entity->getAutorisationParentale(), 'imageFile'));
    }

    public function preUpdate(PreUpdateEventArgs $args) {
        $entity = $args->getEntity();
        if (!$entity instanceof Configuration) {
            return;
        }
        if ($entity->getAutorisationParentale()->getImageFile() instanceof UploadedFile) {
            $this->cacheManager->remove($this->uploaderHelper->asset($entity->getAutorisationParentale(), 'imageFile'));
        }
        if ($entity->getAppLogo()->getImageFile() instanceof UploadedFile) {
            $this->cacheManager->remove($this->uploaderHelper->asset($entity->getAppLogo(), 'imageFile'));
        }
        if ($entity->getBackgroundImage()->getImageFile() instanceof UploadedFile) {
            $this->cacheManager->remove($this->uploaderHelper->asset($entity->getBackgroundImage(), 'imageFile'));
        }
    }

}
