<?php
namespace App\EventListener;

use App\Event\InscriptionEvent;
use App\Mailer\MailerInterface;
use App\AppEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SendMailListener implements EventSubscriberInterface {

    private $mailer;
    private $router;

    /**
     * EmailConfirmationListener constructor.
     *
     * @param MailerInterface         $mailer
     * @param UrlGeneratorInterface   $router
     */
    public function __construct(MailerInterface $mailer, UrlGeneratorInterface $router)
    {
        $this->mailer = $mailer;
        $this->router = $router;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            AppEvents::REGISTRATION_ATELIER_SUCCESS => 'onRegistrationAtelierSuccess',
            AppEvents::REGISTRATION_FORMATION_SUCCESS => 'onRegistrationFormationSuccess',
        );
    }

    /**
     * @param InscriptionEvent $event
     */
    public function onRegistrationAtelierSuccess(InscriptionEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $inscriptionAtelier = $event->getForm()->getData();

        $this->mailer->sendConfirmationEmailMessageAtelier($inscriptionAtelier);
        $this->mailer->sendMailForContacts();
        $url = $this->router->generate('front_inscription_atelier');
        $event->setResponse(new RedirectResponse($url));
    }

    /**
     * @param InscriptionEvent $event
     */
    public function onRegistrationFormationSuccess(InscriptionEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $inscription = $event->getForm()->getData();

        $this->mailer->sendConfirmationEmailMessageFormation($inscription);
        $this->mailer->sendMailForContacts();
        $url = $this->router->generate('front_inscription');
        $event->setResponse(new RedirectResponse($url));
    }
}