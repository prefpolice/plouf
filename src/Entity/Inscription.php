<?php
/**
 * @author julien dechavanne
 * @copyright julien dechavanne
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Inscription
 *
 * @ORM\Table(name="inscription")
 * @ORM\Entity(repositoryClass="App\Repository\InscriptionRepository")
 */
class Inscription
{
    private static $choicesNiveauClasse = [
        '1er degré' => [
            'CM1' => 'cm1',
            'CM2' => 'cm2'
        ],
        'College' => [
            '6ème' => '6eme',
            '5ème' => '5eme'
        ],
        'Autre' => [
            'Groupe périscolaire' => 'groupe périscolaire'
        ]
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Horaire", cascade={"persist"}, inversedBy="inscription")
     */
    protected $horaire;

    /**
     * @var string
     *
     * @ORM\Column(name="nomEtablissement", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nomEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="adresseEtablissement", type="text")
     * @Assert\NotBlank()
     */
    private $adresseEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="nomDirecteur", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nomDirecteur;

    /**
     * @var string
     * @ORM\Column(name="telEtablissement", type="string", length=255)
     * @Assert\Regex(pattern="/(0|\\+33|0033)[1-9][0-9]{8}/", message="Le numéro de téléphone n'est pas au format valide")
     */
    private $telEtablissement;

    /**
     * @var string
     * @ORM\Column(name="emailEtablissement", type="string", length=255)
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas un email valide.",
     *     checkMX = true
     * )
     */
    private $emailEtablissement;

    /**
     * @var string
     * @ORM\Column(name="enseignantNom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $enseignantNom;

    /**
     * @var string
     * @ORM\Column(name="enseignantPrenom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $enseignantPrenom;

    /**
     * @var string
     * @ORM\Column(name="telEnseigant", type="string", length=255, nullable=true)
     * @Assert\Regex(pattern="/(0|\\+33|0033)[1-9][0-9]{8}/", message="Le numéro n'est pas au format valide")
     */
    private $telEnseignant;

    /**
     * @var string
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas un email valide.",
     *     checkMX = true
     * )
     * @ORM\Column(name="enseignantMailPro", type="string", length=255)
     */
    private $enseignantMailPro;

    /**
     * @var string
     * @ORM\Column(name="enseignantMailPerso", type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas un email valide.",
     *     checkMX = true
     * )
     */
    private $enseignantMailPerso;

    /**
     * @var string
     *
     * @ORM\Column(name="classeNiveau", type="string", length=255)
     */
    private $classeNiveau;

    /**
     * @var int
     * @ORM\Column(name="classeNombreEleve", type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 50,
     *      minMessage = "Une classe ne peut avoir moins de {{ limit }} élève",
     *      maxMessage = "Une classe ne peut comporter plus de {{ limit }} élèves"
     * )
     */
    private $classeNombreEleve;

    /**
     * @var string
     *
     * @ORM\Column(name="autreInfos", type="string", length=255, nullable=true)
     */
    private $autreInfos;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomEtablissement
     *
     * @param string $nomEtablissement
     *
     * @return Inscription
     */
    public function setNomEtablissement($nomEtablissement)
    {
        $this->nomEtablissement = $nomEtablissement;

        return $this;
    }

    /**
     * Get nomEtablissement
     *
     * @return string
     */
    public function getNomEtablissement()
    {
        return $this->nomEtablissement;
    }

    /**
     * Set adresseEtablissement
     *
     * @param string $adresseEtablissement
     *
     * @return Inscription
     */
    public function setAdresseEtablissement($adresseEtablissement)
    {
        $this->adresseEtablissement = $adresseEtablissement;

        return $this;
    }

    /**
     * Get adresseEtablissement
     *
     * @return string
     */
    public function getAdresseEtablissement()
    {
        return $this->adresseEtablissement;
    }

    /**
     * Set nomDirecteur
     *
     * @param string $nomDirecteur
     *
     * @return Inscription
     */
    public function setNomDirecteur($nomDirecteur)
    {
        $this->nomDirecteur = $nomDirecteur;

        return $this;
    }

    /**
     * Get nomDirecteur
     *
     * @return string
     */
    public function getNomDirecteur()
    {
        return $this->nomDirecteur;
    }

    /**
     * Set telEtablissement
     *
     * @param string $telEtablissement
     *
     * @return Inscription
     */
    public function setTelEtablissement($telEtablissement)
    {
        $this->telEtablissement = $telEtablissement;

        return $this;
    }

    /**
     * Get telEtablissement
     *
     * @return string
     */
    public function getTelEtablissement()
    {
        return $this->telEtablissement;
    }

    /**
     * Set emailEtablissement
     *
     * @param string $emailEtablissement
     *
     * @return Inscription
     */
    public function setEmailEtablissement($emailEtablissement)
    {
        $this->emailEtablissement = $emailEtablissement;

        return $this;
    }

    /**
     * Get emailEtablissement
     *
     * @return string
     */
    public function getEmailEtablissement()
    {
        return $this->emailEtablissement;
    }

    /**
     * Set enseignantNom
     *
     * @param string $enseignantNom
     *
     * @return Inscription
     */
    public function setEnseignantNom($enseignantNom)
    {
        $this->enseignantNom = $enseignantNom;

        return $this;
    }

    /**
     * Get enseignantNom
     *
     * @return string
     */
    public function getEnseignantNom()
    {
        return $this->enseignantNom;
    }

    /**
     * Set enseignantPrenom
     *
     * @param string $enseignantPrenom
     *
     * @return Inscription
     */
    public function setEnseignantPrenom($enseignantPrenom)
    {
        $this->enseignantPrenom = $enseignantPrenom;

        return $this;
    }

    /**
     * Get enseignantPrenom
     *
     * @return string
     */
    public function getEnseignantPrenom()
    {
        return $this->enseignantPrenom;
    }

    /**
     * Set enseignantMailPro
     *
     * @param string $enseignantMailPro
     *
     * @return Inscription
     */
    public function setEnseignantMailPro($enseignantMailPro)
    {
        $this->enseignantMailPro = $enseignantMailPro;

        return $this;
    }

    /**
     * Get enseignantMailPro
     *
     * @return string
     */
    public function getEnseignantMailPro()
    {
        return $this->enseignantMailPro;
    }

    /**
     * Set enseignantMailPerso
     *
     * @param string $enseignantMailPerso
     *
     * @return Inscription
     */
    public function setEnseignantMailPerso($enseignantMailPerso)
    {
        $this->enseignantMailPerso = $enseignantMailPerso;

        return $this;
    }

    /**
     * Get enseignantMailPerso
     *
     * @return string
     */
    public function getEnseignantMailPerso()
    {
        return $this->enseignantMailPerso;
    }

    /**
     * Set classeNiveau
     *
     * @param string $classeNiveau
     *
     * @return Inscription
     */
    public function setClasseNiveau($classeNiveau)
    {
        $this->classeNiveau = $classeNiveau;

        return $this;
    }

    /**
     * Get classeNiveau
     *
     * @return string
     */
    public function getClasseNiveau()
    {
        return $this->classeNiveau;
    }
    
    /**
     * Set classeNombreEleve
     *
     * @param integer $classeNombreEleve
     *
     * @return Inscription
     */
    public function setClasseNombreEleve($classeNombreEleve)
    {
        $this->classeNombreEleve = $classeNombreEleve;

        return $this;
    }

    /**
     * Get classeNombreEleve
     *
     * @return int
     */
    public function getClasseNombreEleve()
    {
        return $this->classeNombreEleve;
    }

    /**
     * Set autreInfos
     *
     * @param string $autreInfos
     *
     * @return Inscription
     */
    public function setAutreInfos($autreInfos)
    {
        $this->autreInfos = $autreInfos;

        return $this;
    }

    /**
     * Get autreInfos
     *
     * @return string
     */
    public function getAutreInfos()
    {
        return $this->autreInfos;
    }

    /**
     * Set horaire
     *
     * @param Horaire $horaire
     *
     * @return Inscription
     */
    public function setHoraire(Horaire $horaire = null)
    {
        $this->horaire = $horaire;

        return $this;
    }

    /**
     * Get horaire
     *
     * @return Horaire
     */
    public function getHoraire()
    {
        return $this->horaire;
    }

    /**
     * @return string
     */
    public function getTelEnseignant()
    {
        return $this->telEnseignant;
    }

    /**
     * @param string $telEnseignant
     */
    public function setTelEnseignant($telEnseignant)
    {
        $this->telEnseignant = $telEnseignant;
    }

    public static function getClasseNiveaux(){
        return self::$choicesNiveauClasse;
    }
    
}
