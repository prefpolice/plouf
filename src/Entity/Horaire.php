<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Horaire
 *
 * @ORM\Table(name="horaire")
 * @ORM\Entity(repositoryClass="App\Repository\HoraireRepository")
 */
class Horaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_at", type="datetime")
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_at", type="datetime")
     */
    private $endAt;

    /**
     * @var int
     *
     * @ORM\Column(name="nbPlaces", type="integer")
     */
    private $nbPlaces = 4;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_closed", type="boolean")
     */
    private $isClosed;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Inscription", cascade={"persist"}, mappedBy="horaire")
     */
    private $inscription;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Jour",
     *     inversedBy="horaires"
     * )
     * @ORM\OrderBy(value={"id"="ASC"})
     */
    private $jour;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param \DateTime $startAt
     * @return Horaire
     */
    public function setStartAt(\DateTime $startAt): Horaire
    {
        $this->startAt = $startAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param \DateTime $endAt
     * @return Horaire
     */
    public function setEndAt(\DateTime $endAt): Horaire
    {
        $this->endAt = $endAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbPlaces(): int
    {
        return $this->nbPlaces;
    }

    /**
     * @param int $nbPlaces
     */
    public function setNbPlaces(int $nbPlaces): void
    {
        $this->nbPlaces = $nbPlaces;
        if ($this->nbPlaces == 0){
            $this->setIsClosed(true);
        }else{
            $this->setIsClosed(false);
        }
    }

    /**
     * @return bool
     */
    public function getIsClosed(): bool
    {
        return $this->isClosed;
    }

    /**
     * @param bool $isClosed
     */
    public function setIsClosed(bool $isClosed): void
    {
        $this->isClosed = $isClosed;
    }

    /**
     * @return mixed
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * @param mixed $inscription
     */
    public function setInscription($inscription): void
    {
        $this->inscription = $inscription;
    }

    /**
     * @return mixed
     */
    public function getJour()
    {
        return $this->jour;
    }

    /**
     * @param mixed $jour
     */
    public function setJour($jour): void
    {
        $this->jour = $jour;
    }

    public function decrementerPlaces(){
       $this->nbPlaces = $this->nbPlaces-1;
        if ($this->getNbPlaces() == 0){
            $this->setIsClosed(true);
        }
        return $this;
    }

    public function augmenterPlaces(){
        $this->nbPlaces = $this->nbPlaces+1;
        if ($this->getNbPlaces() != 0){
            $this->setIsClosed(false);
        }
        return $this;
    }

    /**
     * Horaire constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->isClosed = false ;
        $this->nbPlaces = 4 ;
        $this->startAt = new \DateTime("now") ;
        $this->endAt = new \DateTime("now") ;
    }

    /**
     * Add inscription
     *
     * @param \App\Entity\Inscription $inscription
     *
     * @return Horaire
     */
    public function addInscription(Inscription $inscription)
    {
        $this->inscription[] = $inscription;

        return $this;
    }

    /**
     * Remove inscription
     *
     * @param Inscription $inscription
     */
    public function removeInscription(Inscription $inscription)
    {
        $this->inscription->removeElement($inscription);
    }

    public function __toString()
    {
        return $this->getStartAt()->format("H:i"). " à ". $this->getEndAt()->format("H:i");
    }
}
