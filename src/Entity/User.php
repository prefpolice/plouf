<?php
// src/AppBundle/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    static $roles_personnalized = [
        'Standard' =>
            [
                "sercom" => 'ROLE_SERCOM'
            ],
        'Avancé' =>
            [
                "dim" => 'ROLE_DIM',
                "Super Admin" => 'ROLE_SUPER_ADMIN'
            ]
    ];
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public static function getPersonnalizedRoles(){
        return User::$roles_personnalized;
    }

    public function getStringifyRole(){
        $role = preg_replace('/ROLE_/', '', $this->getRoles()[0]);
        $role = preg_replace('/_/', ' ', $role);
        return mb_strtolower($role);
    }

    public function __construct()
    {
        parent::__construct();
    }
}