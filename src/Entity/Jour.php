<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jour
 *
 * @ORM\Table(name="jour")
 * @ORM\Entity(repositoryClass="App\Repository\JourRepository")
 */
class Jour
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OrderBy(value={"id"="ASC"})
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="jour", type="date")
     */
    private $jour;
    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Horaire",
     *     mappedBy="jour",
     *     orphanRemoval=true,
     *     cascade={"persist","remove"}
     * )
     */
    private $horaires;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->horaires = new \Doctrine\Common\Collections\ArrayCollection();
        $this->jour = new \DateTime("now");
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param $jour
     * @return $this
     */
    public function setJour($jour)
    {
        $this->jour = $jour;

        return $this;
    }
    /**
     * @return \DateTime
     */
    public function getJour()
    {
        return $this->jour;
    }

    /**
     * Add horaire
     *
     * @param Horaire $horaire
     *
     * @return Jour
     */
    public function addHoraire(Horaire $horaire)
    {
        $horaire->setJour($this);
        $this->horaires->add($horaire);
        return $this;
    }
    /**
     * Remove horaire
     *
     * @param Horaire $horaire
     */
    public function removeHoraire(Horaire $horaire)
    {
        $this->horaires->removeElement($horaire);
    }
    /**
     * Get horaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHoraires()
    {
        return $this->horaires;
    }

}
