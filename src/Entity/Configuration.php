<?php
/**
 * Class Configuration générale de l'application
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationRepository")
 * @Vich\Uploadable()
 */
class Configuration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_open;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $app_title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $message_inscription;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $message_atelier;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $confirmation_inscription;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $confirmation_atelier;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Fichier", cascade={"persist", "remove"})
     */
    private $background_image;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Fichier", cascade={"persist", "remove"})
     */
    private $app_logo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Fichier", cascade={"persist", "remove"})
     */
    private $autorisation_parentale;

    public function __construct()
    {
        $this->is_open = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsOpen(): ?bool
    {
        return $this->is_open;
    }

    public function setIsOpen(bool $is_open): self
    {
        $this->is_open = $is_open;

        return $this;
    }

    public function getAppTitle(): ?string
    {
        return $this->app_title;
    }

    public function setAppTitle(string $app_title): self
    {
        $this->app_title = $app_title;

        return $this;
    }

    public function getMessageInscription(): ?string
    {
        return $this->message_inscription;
    }

    public function setMessageInscription(string $message_inscription): self
    {
        $this->message_inscription = $message_inscription;

        return $this;
    }

    public function getMessageAtelier(): ?string
    {
        return $this->message_atelier;
    }

    public function setMessageAtelier(string $message_atelier): self
    {
        $this->message_atelier = $message_atelier;

        return $this;
    }

    public function getConfirmationInscription(): ?string
    {
        return $this->confirmation_inscription;
    }

    public function setConfirmationInscription(string $confirmation_inscription): self
    {
        $this->confirmation_inscription = $confirmation_inscription;

        return $this;
    }

    public function getConfirmationAtelier(): ?string
    {
        return $this->confirmation_atelier;
    }

    public function setConfirmationAtelier(string $confirmation_atelier): self
    {
        $this->confirmation_atelier = $confirmation_atelier;

        return $this;
    }

    public function getBackgroundImage(): ?Fichier
    {
        return $this->background_image;
    }

    public function setBackgroundImage(?Fichier $background_image): self
    {
        $this->background_image = $background_image;

        return $this;
    }

    public function getAppLogo(): ?Fichier
    {
        return $this->app_logo;
    }

    public function setAppLogo(?Fichier $app_logo): self
    {
        $this->app_logo = $app_logo;

        return $this;
    }

    public function getAutorisationParentale(): ?Fichier
    {
        return $this->autorisation_parentale;
    }

    public function setAutorisationParentale(?Fichier $autorisation_parentale): self
    {
        $this->autorisation_parentale = $autorisation_parentale;

        return $this;
    }
}
