<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Atelier
 *
 * @ORM\Table(name="ateliers")
 * @ORM\Entity(repositoryClass="App\Repository\AtelierRepository")
 */
class Atelier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jour", type="date")
     */
    private $jour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="horaire", type="datetime")
     */
    private $horaire;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_open", type="boolean")
     */
    private $isOpen = true;

    /**
     * @var int
     *
     * @ORM\Column(name="nbPlaces", type="integer")
     */
    private $nbPlaces;

    /**
     * @var InscriptionAtelier[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="App\Entity\InscriptionAtelier",
     *      mappedBy="atelier",
     *      orphanRemoval=true
     * )
     * @ORM\OrderBy({"id": "DESC"})
     */
    private $inscriptions;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Atelier
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Atelier
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set jour
     *
     * @param \DateTime $jour
     *
     * @return Atelier
     */
    public function setJour($jour)
    {
        $this->jour = $jour;

        return $this;
    }

    /**
     * Get jour
     *
     * @return mixed
     */
    public function getJour()
    {
        return $this->jour;
    }

    /**
     * Set horaire
     *
     * @param \DateTime $horaire
     *
     * @return Atelier
     */
    public function setHoraire($horaire)
    {
        $this->horaire = $horaire;

        return $this;
    }

    /**
     * Get horaire
     *
     * @return mixed
     */
    public function getHoraire()
    {
        return $this->horaire;
    }

    /**
     * Set isOpen
     *
     * @param boolean $isOpen
     *
     * @return Atelier
     */
    public function setIsOpen($isOpen)
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    /**
     * Get isOpen
     *
     * @return bool
     */
    public function getIsOpen()
    {
        return $this->isOpen;
    }

    /**
     * Set nbPlaces
     *
     * @param integer $nbPlaces
     */
    public function setNbPlaces($nbPlaces)
    {
        $this->nbPlaces = $nbPlaces;
        if ($this->getNbPlaces() == 0){
            $this->setIsOpen(false);
        }else{
            $this->setIsOpen(true);
        }
    }

    /**
     * Get nbPlaces
     *
     * @return int
     */
    public function getNbPlaces()
    {
        return $this->nbPlaces;
    }

    /**
     * @return InscriptionAtelier[]|ArrayCollection
     */
    public function getInscriptions(){
        return $this->inscriptions;
    }

    public function decrementerPlaces(){
        $this->nbPlaces = $this->nbPlaces-1;
        if ($this->getNbPlaces() == 0){
            $this->setIsOpen(false);
        }
        return $this;
    }

    public function augmenterPlaces(){
        $this->nbPlaces = $this->nbPlaces+1;
        if ($this->getNbPlaces() != 0){
            $this->setIsOpen(true);
        }
        return $this;
    }

    public function __toString()
    {
    	$retour = "Le ". $this->getJour()->format("d m Y")." à ". $this->getHoraire()->format("H:i").
            " - ".$this->getTitle()." - "
            . $this->getNbPlaces(). " place(s) restante(s)";
        return $retour;
    }

    public function __construct()
    {
        $this->jour = new \DateTime();
        $this->horaire = new \DateTime();
        $this->isOpen = true;

    }


}
