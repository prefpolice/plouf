<?php

/*
 * Listing des évenement de l'application plouf
 */

namespace App;

/**
 * Contient tous les évenements liés à l'application
 */
final class AppEvents
{
    /**
     * Evenement déclenché lorsque un utilisateur soumet avec succès le formulaire d'inscription a un atelier
     */
    const REGISTRATION_ATELIER_SUCCESS = 'app.atelier.confirmation.send_email';
    /**
     * Evenement déclenché lorsque un utilisateur soumet avec succès le formulaire d'inscription a une initiation pour les classes
     */
    const REGISTRATION_FORMATION_SUCCESS = 'app.formation.confirmation.send_email';
}
