<?php

/*
 * Fichier qui permet de générer automatiquement un utilisateur admin avec un mot de passe qui sera envoyé par mail
 */

namespace App\Command;

use App\Mailer\MailerInterface;
use App\Utils\ConfigurationManipulator;
use App\Utils\ConfigurationManipulatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;


/**
 * @author julien Dechavanne <j.dechavanne@jdspractice.fr>
 */
class InitCommand extends Command
{
    /**
     * @var ConfigurationManipulatorInterface
     */
    private $configurationManipulator;
    /**
     * @var UserManagerInterface
     */
    private $userManager;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * UserCommand constructor.
     * @param ConfigurationManipulator $configurationManipulator
     * @param UserManagerInterface $userManager
     * @param EntityManagerInterface $entityManager
     * @param MailerInterface $mailer
     */
    public function __construct(ConfigurationManipulator $configurationManipulator, UserManagerInterface $userManager, EntityManagerInterface $entityManager, MailerInterface $mailer)
    {


        $this->configurationManipulator = $configurationManipulator;
        $this->userManager = $userManager;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:start')
            ->setDescription('Creer un utilisateur admin et une configuration par défaut.')
            ->setDefinition(array(
                new InputArgument('email', InputArgument::REQUIRED, 'The email'),
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');
        $password = bin2hex(openssl_random_pseudo_bytes(4));
        if (!empty($email)){
            if(!$this->configurationManipulator->checkIfConfigExist()){
                $this->configurationManipulator->initConfiguration();
                $output->writeln("La configuration par défaut a été chargée");
            }
            $user = $this->userManager->createUser();
            $user->setUsername('admin');
            $user->setPlainPassword($password);
            $user->setRoles(["ROLE_SUPER_ADMIN"]);
            $user->setEmail($email);
            $user->setEnabled(true);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->mailer->sendMailForCreateAdminUser($email, $password);
            $output->writeln("l'utilisateur par défaut a été crée");
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();
        if (!$input->getArgument('email')) {
            $question = new Question('Merci de remplir un email valide pour la création de l\'utilisateur admin : ');
            $question->setValidator(function ($email) {
                if (empty($email)) {
                    throw new \Exception('L\'email ne peut etre vide');
                }

                return $email;
            });
            $questions['email'] = $question;
        }
        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    protected function randomPassword($nb_caracter) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $nb_caracter; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}
