<?php

namespace App\Form;

use App\Entity\Horaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HoraireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startAt',TimeType::class,[
                "label" => "form.horaire.startAt",
                "widget" => "single_text",
                "html5" => false,
                "attr" => [
                    "class" => "timepicker",
                    "data-plugin-timepicker" => "",
                    "data-plugin-options"=> '{"showMeridian":false}'
                ]
            ])
            ->add('endAt',TimeType::class,[
                "label" => "form.horaire.endAt",
                "widget" => "single_text",
                "html5" => false,
                "attr" => [
                    "class" => "timepicker",
                    "data-plugin-timepicker" => "",
                    "data-plugin-options"=> '{"showMeridian":false}'
                ]
            ])
            ->add("nbPlaces",IntegerType::class,[
                "label" => "form.horaire.nb_places"
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Horaire::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'horaire_type';
    }


}
