<?php

namespace App\Form;

use App\Entity\Atelier;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AtelierType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isOpen',ChoiceType::class,[
                "label" => "form.inscription.atelier.isOpen",
                "placeholder" => "form.inscription.atelier.default_choice",
                "choices" => [
                    "form.inscription.atelier.isOpen_true"=>"1",
                    "form.inscription.atelier.isOpen_false"=>"0",
                ]
            ])
            ->add('nbPlaces',IntegerType::class,[
                "label" => "form.inscription.atelier.nbPlaces"
            ])
            ->add('title',TextType::class,[
                "label" => "form.inscription.atelier.title"
            ])
            ->add('description',CKEditorType::class,[
                "label" => "form.inscription.atelier.description"
            ])
            ->add('jour',DateType::class,[
                "label" => "form.inscription.atelier.jour",
                "widget" => "single_text",
                "format" => "dd-MM-yyyy",
                'attr' => [
                    'class' => 'form-control',
                    "data-plugin-datepicker"=> "",
                    "data-plugin-options"=>'{"language":"fr","todayBtn":true,"orientation":"bottom","format":"dd-mm-yyyy"}'
                ],
                'html5' => false,
            ])
            ->add('horaire',TimeType::class,[
                "label" => "form.inscription.atelier.horaire",
                "widget" => "single_text",
                "html5" => false,
                "attr" => [
                    "data-plugin-timepicker" => "",
                    "data-plugin-options"=> '{"showMeridian":false}'
                ]
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Atelier::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'atelier_type';
    }


}
