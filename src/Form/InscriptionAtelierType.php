<?php

namespace App\Form;

use App\Entity\Atelier;
use App\Entity\InscriptionAtelier;
use App\Repository\AtelierRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionAtelierType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('atelier',EntityType::class,[
                'class' => Atelier::class,
                'label' => 'form.atelier.title',
                'query_builder' => function(AtelierRepository $er) {
                    return $er->createQueryBuilder('a')
                        -> where('a.isOpen = true')
                        ->orderBy('a.id','ASC');
                },
                'placeholder' => 'form.inscription.choix',
            ])
            ->add('nom',TextType::class,[
                "label" => "form.atelier.nom",
                "attr" =>[
                    "placeholder" => "form.atelier.nom_placeholder"
                ]
            ])
            ->add('prenom',TextType::class,[
                "label" => "form.atelier.prenom",
                "attr" =>[
                    "placeholder" => "form.atelier.prenom_placeholder"
                ]
            ])
            ->add('email',EmailType::class,[
                "label" => "form.atelier.email",
                "attr" =>[
                    "placeholder" => "form.atelier.email_placeholder"
                ]
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => InscriptionAtelier::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'inscription_atelier_type';
    }


}
