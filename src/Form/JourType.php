<?php

namespace App\Form;

use App\Entity\Horaire;
use App\Entity\Jour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JourType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('jour',DateType::class,[
                "label" => "form.jour.label",
                "widget" => "single_text",
                "format" => "dd-MM-yyyy",
                'attr' => [
                    'class' => 'form-control',
                    "data-plugin-datepicker"=> "",
                    "data-plugin-options"=>'{"language":"fr","todayBtn":"true","orientation":"bottom","format":"dd-mm-yyyy"}'
                ],
                'html5' => false,
        ])
            ->add('horaires', CollectionType::class,[
                "label"=> "form.horaires",
                "entry_type" => HoraireType::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'prototype'    => true,
                'required'     => false,
                'by_reference' => false,
                'attr' => [
                    "label" => false,
                    "class" => "horaire-selector row",
                ],
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Jour::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jour_type';
    }


}
