<?php

namespace App\Form\Configuration;

use App\Entity\Configuration;
use App\Form\FichierType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonalisationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('app_title', TextType::class,[
                'empty_data'  => '',
                "label" => "configuration.form.app_title"
            ])
            ->add("backgroundImage", FichierType::class)
            ->add("appLogo", FichierType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Configuration::class,
        ]);
    }
}
