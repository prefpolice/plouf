<?php

namespace App\Form;

use App\Entity\Configuration;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_open', ChoiceType::class,[
                    "choices" => [
                        "configuration.form.is_open_true" => true,
                        "configuration.form.is_open_false" => false
                    ],
                    "label"=>"configuration.form.is_open",
                    "required" => false
                ]
            )
            ->add('app_title', TextType::class,[
                'empty_data'  => '',
                "label" => "configuration.form.app_title"
           ])
            ->add("messageInscription", CKEditorType::class,[
                'empty_data'  => '',
                "label" => "configuration.form.message_inscription"
            ])
            ->add("messageAtelier", CKEditorType::class,[
                'empty_data'  => '',
                "label" => "configuration.form.message_inscription_atelier"
            ])
            ->add("confirmationInscription", CKEditorType::class,[
                'empty_data'  => '',
                "label" => "configuration.form.confirmation_inscription"
            ])
            ->add("confirmationAtelier", CKEditorType::class,[
                'empty_data'  => '',
                "label" => "configuration.form.confirmation_atelier"
            ])
            ->add("autorisationParentale",FichierType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Configuration::class,
        ]);
    }
}
