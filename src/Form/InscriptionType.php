<?php

namespace App\Form;

use App\Entity\Horaire;
use App\Entity\Inscription;
use App\Entity\Jour;
use App\Repository\HoraireRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('horaire', EntityType::class, array(
                    'class' => Horaire::class,
                    'query_builder' => function(HoraireRepository $er) {
                        return $er->createQueryBuilder('h')
                            -> where('h.isClosed = false');
                    },
                    'group_by' => function(Horaire $horaire){
                        return "le " . $horaire->getJour()->getJour()->format("d m Y");
                    },
                    'label' => 'form.inscription.horaire.nom',
                    'attr' => array('class' => 'form-control'),
                    'placeholder' => 'form.inscription.choix',
                )
            )
            ->add('classeNiveau', ChoiceType::class, [
                    'label' => 'form.inscription.classe.niveau',
                    'choices' => Inscription::getClasseNiveaux(),
                    'placeholder' => 'form.inscription.choix',
                ]
            )
            ->add('nomEtablissement', TextType::class,[
                "label" => "form.inscription.etablissement.nom",
                "attr" => [
                    "placeholder" => "form.inscription.etablissement.nom"
                ]
            ])
            ->add('adresseEtablissement', TextareaType::class,[
                "label" => "form.inscription.etablissement.adresse",
                "attr" => [
                    "placeholder" => "form.inscription.etablissement.adresse"
                ]])
            ->add('nomDirecteur', TextType::class,[
                "label" => "form.inscription.directeur.nom",
                "attr" => [
                    "placeholder" => "form.inscription.directeur.nom"
                ]])
            ->add('telEtablissement', TextType::class,[
                "label" => "form.inscription.etablissement.telephone",
                "attr" => [
                    "placeholder" => "form.inscription.etablissement.telephone"
                ]])
            ->add('emailEtablissement', EmailType::class,[
                "label" => "form.inscription.etablissement.email",
                "attr" => [
                    "placeholder" => "form.inscription.etablissement.email"
                ]])
            ->add('enseignantNom', TextType::class,[
                "label" => "form.inscription.enseignant.nom",
                "attr" => [
                    "placeholder" => "form.inscription.enseignant.nom"
                ]])
            ->add('enseignantPrenom', TextType::class,[
                "label" => "form.inscription.enseignant.prenom",
                "attr" => [
                    "placeholder" => "form.inscription.enseignant.prenom"
                ]])
            ->add('telEnseignant', TextType::class,[
                "label" => "form.inscription.enseignant.telephone",
                "required" => false,
                "attr" => [
                    "placeholder" => "form.inscription.enseignant.telephone"
                ]])
            ->add('enseignantMailPro', EmailType::class,[
                "label" => "form.inscription.enseignant.email_pro",
                "attr" => [
                    "placeholder" => "form.inscription.enseignant.email_pro"
                ]])
            ->add('enseignantMailPerso', EmailType::class,[
                "label" => "form.inscription.enseignant.email_perso",
                "required" => false,
                "attr" => [
                    "placeholder" => "form.inscription.enseignant.email_perso"
                ]])
            ->add('classeNombreEleve', IntegerType::class,[
                "label" => "form.inscription.classe.nombre",
                "attr" => [
                    "placeholder" => "form.inscription.classe.nombre"
                ]])
            ->add('autreInfos', TextType::class,[
                "required" => false,
                "label" => "form.inscription.autre",
                "attr" => [
                    "placeholder" => "form.inscription.autre"
                ]]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Inscription::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'inscription_type';
    }


}
