<?php

/**
 * Interface du Mailer
 */

namespace App\Mailer;

use App\Entity\Inscription;
use App\Entity\InscriptionAtelier;
use App\Repository\ContactRepository;

/**
 * @author julien dechavanne <j.dechavanne@jdspracitce.fr>
 */
interface MailerInterface
{
    /**
     * Envoie un email a l'utilisateur lors de l'inscription à un atelier
     *
     * @param InscriptionAtelier $inscriptionAtelier
     */
    public function sendConfirmationEmailMessageAtelier(InscriptionAtelier $inscriptionAtelier);

    /**
     * Envoie un email a l'utilisateur lors de l'inscription à un atelier
     *
     * @param Inscription $inscription
     */
    public function sendConfirmationEmailMessageFormation(Inscription $inscription);

    /**
     * @param string $email
     * @param string $mdp
     * @return mixed
     */
    public function sendMailForCreateAdminUser($email, $mdp);

    /**
     * @param ContactRepository $repository
     * @return mixed
     * Envoie un email aux contacts de la plateforme lors d'une inscription
     */
    public function sendMailForContacts();

}
