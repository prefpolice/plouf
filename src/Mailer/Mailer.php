<?php
/**
 * Gestion de l'envoie de mail lors des inscriptions
 */
namespace App\Mailer;

use App\Entity\Contact;
use App\Entity\Inscription;
use App\Entity\InscriptionAtelier;
use App\Repository\ConfigurationRepository;
use App\Repository\ContactRepository;
use App\Utils\ConfigurationManipulator;
use App\Utils\ConfigurationManipulatorInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @author julien dechavanne <j.dechavanne@jdspracitce.fr>
 */
class Mailer implements MailerInterface
{
    /**
     * @var string $sender
     */
    protected $sender;
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var \App\Entity\Configuration|null
     */
    protected $configuration;

    protected $contactRepository;
    /**
     * @var ConfigurationManipulatorInterface
     */
    private $configurationManipulator;

    /**
     * Mailer constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param UrlGeneratorInterface $router
     * @param EngineInterface $templating
     * @param string $sender
     * @param TranslatorInterface $translator
     * @param ConfigurationManipulatorInterface $configurationManipulator
     * @param ContactRepository $contactRepository
     */
    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface  $router, EngineInterface $templating, string $sender, TranslatorInterface $translator, ConfigurationManipulatorInterface $configurationManipulator, ContactRepository $contactRepository)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->sender = $sender;
        $this->translator = $translator;
        $this->configurationManipulator = $configurationManipulator;
        $this->contactRepository = $contactRepository;
        $this->configurationManipulator = $configurationManipulator;
    }

    /**
     * {@inheritdoc}
     * Préparation du mail lors de l'inscription a une initiation
     */
    public function sendConfirmationEmailMessageFormation(Inscription $inscription)
    {
        $template= "mails/formation/email.html.twig";
        $body = $this->templating->render($template, ["inscription" => $inscription, "configuration" => $this->configuration]);
        $subject = $this->translator->trans("email.confirmation.title");
        $toEmail = [$inscription->getEmailEtablissement(),$inscription->getEnseignantMailPerso(), $inscription->getEnseignantMailPro()];
        $this->sendEmailMessage($subject, $toEmail, $body);
    }

    /**
     * {@inheritdoc}
     * Préparation du mail lors de l'inscription a un atelier
     */
    public function sendConfirmationEmailMessageAtelier(InscriptionAtelier $inscriptionAtelier)
    {
       $template= "mails/atelier/email.html.twig";
       $body = $this->templating->render($template, ["inscription" => $inscriptionAtelier, "configuration" => $this->configuration]);
       $subject = $this->translator->trans("email.confirmation.title");
       $toEmail = $inscriptionAtelier->getEmail();
       $this->sendEmailMessage($subject, $toEmail, $body);
    }

    /**
     * Email envoyer a l'administratuer de la plateforme lors de l'installation automatique de cette derniere.
     * @param string $email
     * @param string $mdp
     * @return mixed|void
     */
    public function sendMailForCreateAdminUser($email, $mdp)
    {
        $template= "mails/command/email.html.twig";
        $body = $this->templating->render($template, ["password" => $mdp]);
        $subject = $this->translator->trans("email.user_create.title");
        $toEmail = $email;
        $this->sendEmailMessage($subject, $toEmail, $body);
    }

    public function sendMailForContacts(){
        $contacts = $this->contactRepository->findAll();
        $template= "mails/contact/email.html.twig";
        $body = $this->templating->render($template, ["url" => $this->router->generate("homepage", [],UrlGeneratorInterface::ABSOLUTE_URL)]);
        $subject = $this->translator->trans("email.contact.title");
        foreach ($contacts as $contact){
            /**
             * @var $contact Contact
             */
            $this->sendEmailMessage($subject, $contact->getEmail(), $body);
        }
    }

    /**
     * @param $toEmail
     * @param $body
     * @param $subject
     */
    protected function sendEmailMessage($subject, $toEmail, $body)
    {
        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($this->sender)
            ->setTo($toEmail)
            ->setBody($body, "text/html");

        $this->mailer->send($message);
    }
}
