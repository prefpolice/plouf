<?php
namespace App\Utils;

use App\Entity\Configuration;
use App\Repository\ConfigurationRepository;
use Doctrine\ORM\EntityManagerInterface;

class ConfigurationManipulator implements ConfigurationManipulatorInterface {

    const LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at odio dictum risus suscipit vehicula. Nullam consectetur sit amet arcu a hendrerit. Duis vel mollis diam, vel molestie sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla vehicula porttitor neque, vitae pulvinar nibh fringilla ut. Maecenas ac ante feugiat, rutrum nisl sed, suscipit nunc. Integer quis condimentum mauris. Maecenas sed pretium arcu. Sed aliquet facilisis risus. Mauris tristique sapien orci. Pellentesque vulputate vulputate tempus. In sed feugiat velit. Fusce sed condimentum est, vitae sodales orci. ";

    const APP_NAME = "APP";

    /**
     * @var ConfigurationRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    private $configuration;

    /**
     * ConfigurationManipulator constructor.
     * @param ConfigurationRepository $repository
     * @param EntityManagerInterface $manager
     */
    public function __construct(ConfigurationRepository $repository, EntityManagerInterface $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        $this->setConfiguration();
        return $this->configuration;
    }

    /**
     * @return ConfigurationManipulator
     */
    private function setConfiguration()
    {
        $configuration = $this->repository->findOneBy([],['id'=> 'DESC']);
        $this->configuration = $configuration;
        return $this;
    }

    public function checkIfConfigExist(){
        if ($this->getConfiguration() instanceof Configuration){
            return $this->getConfiguration();
        }else{
            return false;
        }
    }

    public function initConfiguration(){
        $configuration = new Configuration();
        $configuration->setIsOpen(false);
        $configuration->setMessageAtelier(self::LOREM_IPSUM);
        $configuration->setMessageInscription(self::LOREM_IPSUM);
        $configuration->setConfirmationAtelier(self::LOREM_IPSUM);
        $configuration->setConfirmationInscription(self::LOREM_IPSUM);
        $configuration->setAppTitle(self::APP_NAME);
        $this->manager->persist($configuration);
        $this->manager->flush();

    }


}