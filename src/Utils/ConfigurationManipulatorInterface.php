<?php
namespace App\Utils;

interface ConfigurationManipulatorInterface{

    /**
     * @return mixed
     * Vérifie qu'une configuration existe, retourne vrai si oui sinon faux
     */
    public function checkIfConfigExist();

    public function initConfiguration();
}