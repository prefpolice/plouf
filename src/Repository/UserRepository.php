<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $role
     *
     * @return array
     */
    public function findByRole($role)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ArrayCollection
     */
    public function getDim(){
        $query = $this->createQueryBuilder("users")
            ->select("users")
            ->getQuery();
        $users = $query->getResult();

        $dim_users = new ArrayCollection();
        foreach ($users as $user){
            /**
             * @var User $user
             */
            if($user->hasRole("ROLE_DIM") or $user->hasRole("ROLE_ADMIN")){
                $dim_users->add($user);
            }
        }
        return $dim_users;
    }

    /**
     * @return ArrayCollection
     */
    public function getSercom(){
        $query = $this->createQueryBuilder("users")
            ->select("users")
            ->getQuery();
        $users = $query->getResult();

        $sercom_users = new ArrayCollection();
        foreach ($users as $user){
            /**
             * @var User $user
             */
            if($user->hasRole("ROLE_SERCOM")){
                $sercom_users->add($user);
            }
        }
        return $sercom_users;
    }
}