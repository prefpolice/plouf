<?php

namespace App\Menu;

use App\Repository\MentionLegalRepository;
use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MenuBuilder
{
    private $factory;
    private $authorizationChecker;
    private $mentionsrepository;

    /**
     * MenuBuilder constructor.
     * @param FactoryInterface $factory
     * @param AuthorizationCheckerInterface $authorizationChecker
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker, MentionLegalRepository $repository)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
        $this->mentionsrepository = $repository;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root')
        ->setChildrenAttribute("class","nav nav-pills");

        $menu->addChild('main_menu.home', ['route' => 'homepage']);
        $menu->addChild('main_menu.jours', ['route' => 'jour_index']);
        $menu->addChild('main_menu.ateliers', ['route' => 'atelier_index']);
        if ($this->authorizationChecker->isGranted("ROLE_ADMIN")){
            $menu->addChild('main_menu.contact',["route" =>"contact_index"]);
            $menu->addChild('main_menu.users',["route" =>"user_list"]);
            $menu->addChild('main_menu.configuration',["route" =>"configuration_index"]);
            $menu->addChild('main_menu.personalisation', ["route"=>"personalisation_index"]);
        }
// ... add more children

        return $menu;
    }

    public function createFrontMenu(array $option){
        $mention = $this->mentionsrepository->findOneBy([],["id"=>"DESC"]);
        $menu = $this->factory->createItem('root')
            ->setChildrenAttribute("class","nav nav-pills");
        $menu->addChild('front_menu.inscription', ['route' => 'front_inscription']);
        $menu->addChild('front_menu.inscription_atelier',['route' => 'front_inscription_atelier']);
        if (!is_null($mention)){
            $menu->addChild('front_menu.mentions',['route' => 'mention_legal_show']);
        }
        return $menu;
    }
}