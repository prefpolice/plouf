<?php
namespace App\Controller;

use App\Repository\MentionLegalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mentions")
 * Class MentionLegaleController
 * @package App\Controller
 */
class MentionLegaleController extends AbstractController{
    /**
     * @Route("/", name="mention_legal_show", methods={"GET"})
     * @param MentionLegalRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function display(MentionLegalRepository $repository){
        $mention = $repository->findOneBy([],["id"=>"DESC"]);
        return $this->render("front/pages/page.html.twig",[
            "mention" => $mention
        ]);
    }
}