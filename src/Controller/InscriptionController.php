<?php
/**
 * Controller pour la gestion des initiations
 * Affiche le formulaire d'inscription
 * Traite l'inscription et l'enregistrement en base
 * @author julien dechavanne
 * @copyright julien dechavanne <j.dechavanne@gmail.com>
 */
namespace App\Controller;

use App\AppEvents;
use App\Entity\Inscription;
use App\Event\InscriptionEvent;
use App\Form\InscriptionType;
use App\Repository\ConfigurationRepository;
use App\Utils\ConfigurationManipulatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Inscription controller.
 *
 * @Route("/inscription")
 */
class InscriptionController extends Controller
{

    private $eventDispatcher;
    /**
     * @var ConfigurationManipulatorInterface
     */
    private $configurationManipulator;


    /**
     * InscriptionController constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param ConfigurationManipulatorInterface $configurationManipulator
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, ConfigurationManipulatorInterface $configurationManipulator)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->configurationManipulator = $configurationManipulator;
    }

    /**
     * Affichage du formulraire d'inscription pour les initiations
     * @param Request $request
     * @Route("/", name="inscription_new", methods={"POST","GET"})
     * @Route("/", name="front_inscription", methods={"POST","GET"})
     * @return Response
     */
    public function newAction(Request $request) : Response
    {
            $configuration = $this->configurationManipulator->checkIfConfigExist();
            $inscription = new Inscription();
            $form = $this->createForm(InscriptionType::class, $inscription);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $inscription->getHoraire()->decrementerPlaces();
                $em->persist($inscription);
                $em->persist($inscription->getHoraire());
                $em->flush();
                $event = new InscriptionEvent($form, $request);
                $this->eventDispatcher->dispatch(AppEvents::REGISTRATION_FORMATION_SUCCESS, $event);
                return $this->render("front/formation/confirmation.html.twig", [
                    "inscription" => $inscription,
                    "configuration" => $configuration
                ]);
            }

            return $this->render('front/formation/inscription.html.twig', array(
                'inscription' => $inscription,
                'form' => $form->createView(),
                "configuration" => $configuration
            ));
    }


}
