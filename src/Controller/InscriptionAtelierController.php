<?php

namespace App\Controller;

use App\AppEvents;
use App\Entity\Atelier;
use App\Entity\InscriptionAtelier;
use App\Event\InscriptionEvent;
use App\Form\InscriptionAtelierType;
use App\Repository\AtelierRepository;
use App\Repository\ConfigurationRepository;
use App\Utils\ConfigurationManipulatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Inscription controller.
 *
 * @Route("/inscription/atelier")
 */
class InscriptionAtelierController extends AbstractController
{
    private $eventDispatcher;
    /**
     * @var ConfigurationManipulatorInterface
     */
    private $configurationManipulator;


    /**
     * InscriptionAtelierController constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param ConfigurationManipulatorInterface $configurationManipulator
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, ConfigurationManipulatorInterface $configurationManipulator)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->configurationManipulator = $configurationManipulator;
    }

    /**
     * Creates a new inscription entity.
     * @param EntityManagerInterface $manager
     * @param  $request Request
     * @Route("/", name="front_inscription_atelier", methods={"POST","GET"})
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $manager) : Response
    {
            $configuration = $this->configurationManipulator->checkIfConfigExist();
            $inscription = new InscriptionAtelier();
            $form = $this->createForm(InscriptionAtelierType::class, $inscription);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $inscription->getAtelier()->decrementerPlaces();
                $manager->persist($inscription);
                $manager->flush();
                $event = new InscriptionEvent($form, $request);
                $this->eventDispatcher->dispatch(AppEvents::REGISTRATION_ATELIER_SUCCESS, $event);
                return $this->render("front/atelier/confirmation.html.twig", [
                    'inscription' => $inscription,
                    'configuration' => $configuration
                ]);
            }
            return $this->render('front/atelier/inscription.html.twig', array(
                'inscription' => $inscription,
                'form' => $form->createView(),
                'configuration' => $configuration
            ));
    }

    /**
     * Ajax rapide pour renvoyer un card avec les infos sur un atelier (a voir pour un retour JSON )
     * @Route("/getInfos/{id}", methods={"POST"}, options={"expose"=true}, name="atelier_get_infos")
     * @param Atelier $atelier
     * @param AtelierRepository $repository
     * @return Response
     */
    public function getAtelierInfos(Atelier $atelier, AtelierRepository $repository){
        $atelier = $repository->find($atelier);
        return $this->render("front/atelier/infos_atelier.html.twig",["atelier"=>$atelier]);
    }
}
