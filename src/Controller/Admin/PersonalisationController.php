<?php

namespace App\Controller\Admin;

use App\Entity\Configuration;
use App\Form\Configuration\PersonalisationType;
use App\Repository\ConfigurationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PersonalisationController
 * @package App\Controller
 * @Route("/admin/personnalisation")
 */
class PersonalisationController extends AbstractController
{

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * PersonalisationController constructor.
     * @param ConfigurationRepository $configurationRepository
     */
    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * @Route("/", name="personalisation_index")
     */
    public function index()
    {
        $configuration = $this->configurationRepository->findOneBy([],["id"=>'DESC']);
        if (!$configuration){
            $this->addFlash("info","Aucune configuration existante, merci d'en créer une afin d'utiliser la plateforme");
            return $this->redirectToRoute("configuration_create");
        }
        return $this->render('back/personalisation/index.html.twig', [
            "configuration" => $configuration
        ]);
    }

    /**
     * @param Request $request
     * @param Configuration $configuration
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/edit/{id}", name="personalisation_personalize")
     */
    public function personalize(Request $request, Configuration $configuration, EntityManagerInterface $manager){
        $form = $this->createForm(PersonalisationType::class, $configuration);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
               $manager->persist($configuration);
               $manager->flush();
               $this->addFlash("success", "La configuration a été modifiée avec succès !");
               return $this->redirectToRoute("personalisation_index");
        }
        return $this->render("back/personalisation/edit.html.twig",[
            "form" => $form->createView(),
            "configuration" => $configuration
        ]);
    }
}
