<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Horaire;
use App\Entity\Jour;
use App\Form\HoraireType;

/**
 * @Route("/admin/horaire")
 */
class HoraireController extends AbstractController
{

    /**
     * @Route("/new/{id}", name="horaire_new", methods="GET|POST")
     * @param Request $request
     * @param Jour $jour
     * @return Response
     */
    public function new(Request $request, Jour $jour): Response
    {
        $horaire = new Horaire();
        $horaire->setJour($jour);
        $horaire->setHoraire($jour->getJour());
        $form = $this->createForm(HoraireType::class, $horaire);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($horaire);
            $em->flush();
            $this->addFlash("success","Horaire ajouté avec succès");
            return $this->redirectToRoute('jour_index');
        }

        return $this->render('back/horaire/new.html.twig', [
            'jour' => $jour,
            'horaire' => $horaire,
            'form' => $form->createView(),
        ]);
    }
}
