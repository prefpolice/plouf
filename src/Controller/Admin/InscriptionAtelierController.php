<?php
namespace App\Controller\Admin;

use App\Entity\InscriptionAtelier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InscriptionAtelierController
 * @package App\Controller\Admin
 * @Route("/admin/inscription_atelier")
 */
class InscriptionAtelierController extends AbstractController{

    /**
     * @Route("/delete/{id}", name="inscription_atelier_delete", methods={"DELETE"})
     * @param Request $request
     * @param InscriptionAtelier $inscriptionAtelier
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, InscriptionAtelier $inscriptionAtelier, EntityManagerInterface $manager){
        if ($this->isCsrfTokenValid('delete'.$inscriptionAtelier->getId(), $request->request->get('_token'))) {
            $inscriptionAtelier->getAtelier()->augmenterPlaces();
            $manager->remove($inscriptionAtelier);
            $manager->flush();
            $this->addFlash("success", "Inscription supprimée avec succès !");
        }
            return $this->redirectToRoute("homepage");
    }
}