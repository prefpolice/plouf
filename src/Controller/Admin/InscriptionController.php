<?php
/**
 * @author julien dechavanne
 * @copyright julien dechavanne
 */
namespace App\Controller\Admin;

use App\Entity\Inscription;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class ConfigurationController
 * @package App\Controller
 * @Route("admin/inscription", methods={"GET"})
 */
class InscriptionController extends AbstractController
{
    /**
     * @Route("/{id}", name="inscription_show")
     * @param Inscription $inscription
     * @return Response
     */
    public function show(Inscription $inscription) : Response{
        return $this->render("back/inscription/show.html.twig",[
            "inscription" => $inscription
        ]);
    }
    /**
     * @Route("/{id}", name="inscription_delete", methods="DELETE")
     * @param Request $request
     * @param Inscription $inscription
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Request $request, Inscription $inscription,EntityManagerInterface $manager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$inscription->getId(), $request->request->get('_token'))) {
            $horaire = $inscription->getHoraire();
            $horaire->augmenterPlaces();
            $manager->persist($horaire);
            $manager->remove($inscription);
            $manager->flush();
            $this->addFlash("success", "Inscription supprimé avec succès");
        }
        return $this->redirectToRoute('homepage');
    }
}