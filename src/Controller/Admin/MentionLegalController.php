<?php

namespace App\Controller\Admin;

use App\Entity\MentionLegal;
use App\Form\MentionLegalType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/mentionLegal")
 */
class MentionLegalController extends AbstractController
{
    /**
     * @Route("/new", name="mention_legal_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $mentionLegal = new MentionLegal();
        $form = $this->createForm(MentionLegalType::class, $mentionLegal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($mentionLegal);
            $em->flush();

            return $this->redirectToRoute('configuration_index');
        }

        return $this->render('back/mention_legal/new.html.twig', [
            'mention_legal' => $mentionLegal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mention_legal_edit", methods="GET|POST")
     * @param Request $request
     * @param MentionLegal $mentionLegal
     * @return Response
     */
    public function edit(Request $request, MentionLegal $mentionLegal): Response
    {
        $form = $this->createForm(MentionLegalType::class, $mentionLegal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mention_legal_edit', ['id' => $mentionLegal->getId()]);
        }

        return $this->render('back/mention_legal/edit.html.twig', [
            'mention_legal' => $mentionLegal,
            'form' => $form->createView(),
        ]);
    }
}
