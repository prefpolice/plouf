<?php

namespace App\Controller\Admin;

use App\Entity\Atelier;
use App\Form\AtelierType;
use App\Repository\AtelierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/atelier")
 */
class AtelierController extends AbstractController
{
    /**
     * @Route("/", name="atelier_index", methods="GET")
     * @param AtelierRepository $atelierRepository
     * @return Response
     */
    public function index(AtelierRepository $atelierRepository): Response
    {
        return $this->render('back/atelier/index.html.twig', [
            'ateliers' => $atelierRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="atelier_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $atelier = new Atelier();
        $form = $this->createForm(AtelierType::class, $atelier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($atelier);
            $em->flush();

            return $this->redirectToRoute('atelier_index');
        }

        return $this->render('back/atelier/new.html.twig', [
            'atelier' => $atelier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="atelier_show", methods="GET")
     * @param Atelier $atelier
     * @return Response
     */
    public function show(Atelier $atelier): Response
    {
        return $this->render('back/atelier/show.html.twig', [
            'atelier' => $atelier
        ]);
    }

    /**
     * @Route("/{id}/edit", name="atelier_edit", methods="GET|POST")
     * @param Request $request
     * @param Atelier $atelier
     * @return Response
     */
    public function edit(Request $request, Atelier $atelier): Response
    {
        $form = $this->createForm(AtelierType::class, $atelier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Atelier modifié avec succès");
            return $this->redirectToRoute('atelier_index');
        }

        return $this->render('back/atelier/edit.html.twig', [
            'atelier' => $atelier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="atelier_delete", methods="DELETE")
     * @param Request $request
     * @param Atelier $atelier
     * @return Response
     */
    public function delete(Request $request, Atelier $atelier): Response
    {
        if ($this->isCsrfTokenValid('delete'.$atelier->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($atelier);
            $em->flush();
        }
        $this->addFlash("success", "Atelier supprimé avec succès");
        return $this->redirectToRoute('atelier_index');
    }
}
