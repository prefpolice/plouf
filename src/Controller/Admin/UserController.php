<?php
/**
 * @author julien dechavanne
 * @copyright julien dechavanne
 */

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\EditUserType;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller\Admin
 * @Route("/users")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends AbstractController {

    /**
     * @Route("/", methods={"GET"}, name="user_list")
     */
    public function list() : Response {
        $all_users = $this
            ->getDoctrine()
            ->getRepository("App:User")
            ->findAll();
        return $this->render("back/user/list.html.twig",[
            'users' => $all_users
        ]);
    }

    /**
     * @param Request $request
     * @Route("/create", name="user_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @return Response
     */
    public function create(Request $request) : Response
    {
        $user = new User();
        $roles = User::getPersonnalizedRoles();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->add('roles', ChoiceType::class,[
            'choices' => $roles,
            'required' => true,
            'multiple' => true,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Utilisateur crée avec succès');
            return $this->redirectToRoute("user_list");
        }
        return $this->render("back/user/create.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
    * @param User $user
    * @param Request $request
    * @return \Symfony\Component\HttpFoundation\Response
    * @Route("/edit/{id}", name="user_edit")
    * @Security("has_role('ROLE_ADMIN')")
    */
    public function editUserAction(User $user, Request $request) : Response{
        $formulaire = $this->createForm(EditUserType::class, $user);
        $formulaire->handleRequest($request);
        if ($formulaire->isSubmitted() && $formulaire->isValid()){
            // faire l'enregistrement + le flush
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Utilisateur modifié avec succès');
            return $this->redirectToRoute("user_list");
        }
        return $this->render("back/user/edit.html.twig",[
            "user" => $user,
            "form" => $formulaire->createView()
        ]);
    }

    /**
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/delete/{id}", name="user_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * */
    public function deleteUserAction(User $user) : RedirectResponse{
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        $this->addFlash('warning', 'Utilisateur supprimé avec succès');
        return $this->redirectToRoute("user_list");
    }


}