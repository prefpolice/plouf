<?php
/**
 * @author julien dechavanne
 * @copyright julien dechavanne
 */
namespace App\Controller\Admin;

use App\Entity\Configuration;
use App\Form\ConfigurationType;
use App\Repository\ConfigurationRepository;
use App\Repository\MentionLegalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Administration de la configuration de l'application
 * Class ConfigurationController
 * @package App\Controller\Admin
 * @Route("/admin/configuration")
 */
class ConfigurationController extends AbstractController{
    /**
     * Affichage de la configuration en cours, si non, affiche un message d'avertissement grace au Configuration Interface
     * @Route("/", name="configuration_index")
     * @param MentionLegalRepository $legalRepository
     * @param ConfigurationRepository $configurationRepository
     * @return Response
     */
    public function index(MentionLegalRepository $legalRepository, ConfigurationRepository $configurationRepository) : Response{
        $mention = $legalRepository->findOneBy([],["id"=>"DESC"]);
        $configuration = $configurationRepository->findOneBy([],['id'=>'DESC']);
        if (!$configuration){
            $this->addFlash("info", "Aucune configuration existante, merci d'en créer une afin d'utiliser la plateforme");
            return $this->redirectToRoute('configuration_create');
        }
        return $this->render("back/configuration/index.html.twig",[
            "mention" => $mention,
            "configuration" => $configuration
        ]);
    }

    /**
     * Formulaire de création de configuration
     * @Route("/create", name="configuration_create", methods={"POST","GET"})
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function create(Request $request, EntityManagerInterface $manager){
        $configuration = new Configuration();
        $form = $this->createForm(ConfigurationType::class, $configuration);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager->persist($configuration);
            $manager->flush();
            $this->addFlash("success", "La configuration a été enregitrée avec succès");
            return $this->redirectToRoute("configuration_index");
        }
        return $this->render("back/configuration/new.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * Formulaire de création de configuration
     * @Route("/edit/{id}", name="configuration_edit", methods={"POST","GET"})
     * @param Request $request
     * @param Configuration $configuration
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, EntityManagerInterface $manager, Configuration $configuration){
        $form = $this->createForm(ConfigurationType::class, $configuration);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager->persist($configuration);
            $manager->flush();
            $this->addFlash("success", "La configuration a été modifiée avec succès");
            return $this->redirectToRoute("configuration_index");
        }
        return $this->render("back/configuration/edit.html.twig", [
            "form" => $form->createView()
        ]);
    }
}