<?php

namespace App\Controller\Admin;

use App\Repository\InscriptionAtelierRepository;
use App\Repository\InscriptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BaseController
 * @package App\Controller\Admin
 * @Route("/admin")
 */
class BaseController extends AbstractController {

    /**
     * @Route("/", name="homepage")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function dashboard(InscriptionRepository $inscriptionRepository, InscriptionAtelierRepository $inscriptionAtelierRepository){
            $list_inscriptions = $inscriptionRepository->findAll();
            $list_inscriptions_atelier = $inscriptionAtelierRepository->findAll();
            return $this->render("back/homepage.html.twig",[
                "inscriptions" => $list_inscriptions,
                "inscriptions_ateliers" => $list_inscriptions_atelier
            ]);
    }
}