<?php
/**
 * @author julien dechavanne
 * @copyright julien dechavanne
 */
namespace App\Controller\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Jour;
use App\Form\JourType;
use App\Repository\JourRepository;

/**
 * @Route("/admin/jour")
 */
class JourController extends AbstractController
{
    /**
     * @Route("/", name="jour_index", methods="GET")
     * @param JourRepository $jourRepository
     * @return Response
     */
    public function index(JourRepository $jourRepository): Response
    {
        return $this->render('back/jour/index.html.twig', [
            'jours' => $jourRepository->findAll()
        ]);
    }
    /**
     * @Route("/new", name="jour_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $jour = new Jour();
        $form = $this->createForm(JourType::class, $jour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($jour);
            $em->flush();

            return $this->redirectToRoute('jour_index');
        }

        return $this->render('back/jour/new.html.twig', [
            'jour' => $jour,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/{id}", name="jour_show", methods="GET")
     * @param Jour $jour
     * @return Response
     */
    public function show(Jour $jour): Response
    {
        return $this->render('back/jour/show.html.twig', ['jour' => $jour]);
    }
    /**
     * @Route("/edit/{id}", name="jour_edit", methods="GET|POST")
     * @param Request $request
     * @param Jour $jour
     * @return Response
     */
    public function edit(Request $request, Jour $jour, EntityManagerInterface $manager): Response
    {
        $originalHoraires = new ArrayCollection();
        /**
         * On stock les horaires initiaux lors de l'arrivée sur la page d'edition
         */
        foreach ($jour->getHoraires() as $horaire){
            $originalHoraires->add($horaire);
        }
        $form = $this->createForm(JourType::class, $jour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * On compare les nouveaux horaires avec les anciens, s'il y a différence, on supprime (dans le cas contraire
             * c'est sauvegardé automatiquement)
             */
            foreach ($originalHoraires as $horaire){
                if (false === $jour->getHoraires()->contains($horaire)){
                    $jour->getHoraires()->removeElement($horaire);
                    $manager->remove($horaire);
                }
            }
            $manager->persist($jour);
            $manager->flush();
            return $this->redirectToRoute('jour_index');
        }

        return $this->render('back/jour/edit.html.twig', [
            'jour' => $jour,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/{id}", name="jour_delete", methods="DELETE")
     * @param Request $request
     * @param Jour $jour
     * @return Response
     */
    public function delete(Request $request, Jour $jour): Response
    {
        if ($this->isCsrfTokenValid('delete'.$jour->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($jour);
            $em->flush();
        }
        $this->addFlash("success", "Jour de formation supprimé avec succès !");
        return $this->redirectToRoute('jour_index');
    }
}
