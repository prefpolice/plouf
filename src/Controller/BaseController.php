<?php
/**
 * @author julien dechavanne
 * @copyright julien dechavanne<j.dechavanne@jdspractice.fr>
 */
namespace App\Controller;

use App\Utils\ConfigurationManipulatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BaseController
 * @package App\Controller
 * @Route("/")
 */
class BaseController extends AbstractController{

    /**
     * @param ConfigurationManipulatorInterface $config
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="front_homepage")
     */
    public function homepage(ConfigurationManipulatorInterface $config)
    {
        $configuration = $config->checkIfConfigExist();
        return $this->render("front/homepage.html.twig",[
            'configuration' => $configuration
        ]);
    }
}
